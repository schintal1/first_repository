package com.jpm.selenium.forms;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.jpm.selenium.util.ChromeUtil;

public class WorkingWithForms {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		//Open the page
		String url = "file:\\F:\\Java\\Suresh\\SeleniumWorkSpace\\11_SeleniumWebDriver\\src\\main\\java\\WorkingWithForms.html";
		driver.get(url);
		Thread.sleep(2000);
		driver.manage().window().maximize();
		
		driver.findElement(By.id("txtUserName")).sendKeys("Suresh");
		driver.findElement(By.name("txtPwd")).sendKeys("Jpm123");
		driver.findElement(By.className("Format")).sendKeys("Jpm123");
		driver.findElement(By.cssSelector("input.Format1")).sendKeys("Surya");
		driver.findElement(By.cssSelector("input#txtLastName")).sendKeys("Chandra");
		
		
		List<WebElement> radioElements = driver.findElements(By.name("gender"));
		for(WebElement radio:radioElements){
			String radioSelection = radio.getAttribute("value").toString();
			if(radioSelection.equals("Male")){
				radio.click();
			}
		}
		
		driver.findElement(By.id("DOB")).sendKeys("16-06-1995");
		driver.findElement(By.id("txtEmail")).sendKeys("email@send.com");
		
		//drop down
		driver.findElement(By.name("Address")).sendKeys("Mumbai");;
		Select drpCity = new Select(driver.findElement(By.name("City")));
		//Drop Down 3 Ways -> select by Index, Select By Value, select By VisibleTet
		drpCity.selectByIndex(3);
		drpCity.selectByValue("Mumbai");
		drpCity.selectByVisibleText("Pune");
		
		driver.findElement(By.id("txtPhone")).sendKeys("8880299889");
		
		List<WebElement> checkEleList = driver.findElements(By.name("chkHobbies"));
		for (WebElement hobbyChkBox:checkEleList){
			String selection = hobbyChkBox.getAttribute("value").toString();
			if((!selection.equals("Movies"))){
				hobbyChkBox.click();
			}
		}
		
		
		Thread.sleep(5000);
		driver.close();
	}

}
