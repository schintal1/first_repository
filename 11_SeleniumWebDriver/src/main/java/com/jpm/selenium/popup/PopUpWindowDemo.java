package com.jpm.selenium.popup;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.jpm.selenium.util.ChromeUtil;

public class PopUpWindowDemo {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		//Open the page
		String url = "file:\\F:\\Java\\Suresh\\SeleniumWorkSpace\\11_SeleniumWebDriver\\src\\main\\java\\PopUpWinDemo.html";
		driver.get(url);
		
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.id("newtab")).click();
		String parentWin = driver.getWindowHandle();
		System.out.println("I am at parent Window" +parentWin);
		
		Set<String> childWinList = driver.getWindowHandles(); //all the window used
		for(String childWin : childWinList){
		
			if (!childWin.equals(parentWin)){
				Thread.sleep(2000);
				driver.switchTo().window(childWin);
				System.out.println("I am at child Window" +childWin);
				driver.findElement(By.id("alert")).click();
				Thread.sleep(2000);
				
				
				break;
			}
			
		}
			
		driver.switchTo().window(parentWin);
		
		System.out.println("Switch to parent Win :" +parentWin);
		Thread.sleep(2000);
		System.out.println("Again click");
		
		driver.findElement(By.id("newwindow")).click();
		for(String childWin2: driver.getWindowHandles()){
			if(!childWin2.equals(parentWin)){
				driver.switchTo().window(childWin2);
				System.out.println("Child Win 2: "+ childWin2);
				Thread.sleep(2000);
				break;
			}
		}
			
		
		//Thread.sleep(2000);
		driver.quit();
	}

}
