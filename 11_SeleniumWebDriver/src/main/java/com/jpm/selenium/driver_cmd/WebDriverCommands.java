package com.jpm.selenium.driver_cmd;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class WebDriverCommands {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = ChromeUtil.getChromeDriver();
		if(driver!=null){
			//get ->to Launch the WebSite
			//String url = "http://www.synergetics-india.com/";
			String url = "file:\\F:\\Java\\Suresh\\SeleniumWorkSpace\\11_SeleniumWebDriver\\src\\main\\java\\Locators.html";
			driver.get(url);
			System.out.println("Title of the Page is : " + driver.getTitle());
			System.out.println("Geting current URL : "+driver.getCurrentUrl());
			// By.id
			WebElement element = driver.findElement(By.id("user"));
			System.out.println("Element :" + element);
			element.sendKeys("admin");
			driver.findElement(By.name("admin")).sendKeys("password@123");
			//driver.findElement(By.linkText("How to use locators?")).click();
			element=driver.findElement(By.linkText("How to use locators?"));
			System.out.println("Element in the Link is : "+element.getText());
			element.click();
			
			
		}else{
			System.out.println("Driver Not Loaded");
		}
         
		Thread.sleep(2000);
		driver.close();
	}

}
