package com.jpm.selenium.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeUtil {

	public static WebDriver getChromeDriver(){
		WebDriver driver = null;
		//Step1 driver Class
		String driverClassKey="webdriver.chrome.driver";
		//driver path
		String driverPath = ".\\driver\\chromedriver.exe";
		//System Class
		System.setProperty(driverClassKey, driverPath);
		//Set the Chrome Options
		ChromeOptions options = new ChromeOptions();
		//Get the Driver Instance
		driver= new ChromeDriver(options);
		System.out.println("Trying to load Chrome Browser");
		if(driver != null){
			System.out.println("Driver  Loaded !!...");
		}
		
		return driver;
	}
	
	
	public static void main(String[] args) {
		
		WebDriver driver = ChromeUtil.getChromeDriver();
		System.out.println("Opening the Browser :" +driver);
		System.out.println("Closing the Browser " );
		driver.close();

	}

	//get
	//getCurrentUrl
	//findElement
	//getPageSource
	//getTitle
	//close()
	//quite() - Close all the instance of Browser created be Webdriver.
	//getWindowHandles()
	//switchTo()
	//navigate() - to do browser navigation - back, forward , to refresh
	
	
}
