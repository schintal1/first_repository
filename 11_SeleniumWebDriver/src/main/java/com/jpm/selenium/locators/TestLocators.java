package com.jpm.selenium.locators;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class TestLocators {

	public static void main(String[] args) throws InterruptedException {

    //Load Driver
		WebDriver driver = ChromeUtil.getChromeDriver();
		//Open the page
		String url = "http://demo.opencart.com";
		driver.get(url);
		Thread.sleep(2000);
		WebElement searchBox = driver.findElement(By.name("search"));
		searchBox.sendKeys("Phone");
		Thread.sleep(2000);
		driver.findElement(By.className("input-group-btn")).click();
		Thread.sleep(10000);
		
		WebElement searchBox1 = driver.findElement(By.id("input-search"));
		searchBox1.clear();
		searchBox1.sendKeys("Mac");
			
		driver.findElement(By.xpath("//*[@id=\'button-search\']")).click();
		Thread.sleep(2000);
		driver.navigate().back();
		
		//by Tag name
		List<WebElement> listAnchor = driver.findElements(By.tagName("a"));
		
		System.out.println("List of 'a' tags");
//		for(WebElement temp:listAnchor){
//			System.out.println("Text Name: " + temp.getText());
//		}
		
		listAnchor.forEach(temp->System.out.println(temp.getText()));
	//	listAnchor.forEach(System.out::println);
		
		
		//by.cssSelector
		List<WebElement> listCss = driver.findElements(By.cssSelector("span.price-tax"));
		System.out.println("List of Css Selectors:");
		
		listCss.forEach(action->System.out.println(action.getText()));
		
		Thread.sleep(5000);
		
		
		driver.close();

	}

}
